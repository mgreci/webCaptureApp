import { Meteor } from 'meteor/meteor';

Meteor.methods({
  'webshot' : function(url) {
    var webshot = Npm.require('webshot');
    var fs = Npm.require('fs');
    var Future = Npm.require('fibers/future');
    var fut = new Future();

    var fileName = "result.pdf";
    var webshotOptions = {
      //windowSize option changes the size of the browser window
      //PhantomJS uses to capture the given URL
      /*
      "windowSize" : {
        width: 1024,
        height: 768
      },
      */
      //shotSize option changes the size of the PDF capture
      /*
      "shotSize" : {
        width: 1024,
        height: 768
      },
      */
      //zoomFactor changes the amount of zoom (1.0 being 100%)
      /*
      "zoomFactor" : 0.5,
      */
      "streamType" : "pdf",
      "paperSize": {
        //select between Letter or Tabloid
        "format": "Tabloid",
        "orientation": "portrait",
        "margin": "1cm"
      }
    };

    //Make webshot call
    webshot(url, fileName, webshotOptions, function(error) {
      //Log webshot error
      if (error) {
        console.log(error);
        return;
      }
      //Read file
      fs.readFile(fileName, function (err, data) {
        //Log read file error
        if (err) {
          return console.log(err);
        }

        fs.unlinkSync(fileName);
        fut.return(data);
      });
    });

    //Obtain PDF data and convert to base64String
    var pdfData = fut.wait();
    var base64String = new Buffer(pdfData).toString('base64');

    return base64String;
  }
});
