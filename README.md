# webCaptureApp

##### Matthew Greci, December 4, 2017

---

### Introduction

This application is designed to generate a PDF from a given URL.

The front-end UI will accept the URL and upon clicking the PDF button,
the server will use the npm package "webshot" (which utilizes PhantomJS) to
generate a PDF image from the user entered URL and return the PDF in the form
of a string that most modern browsers can then download and save to disk.

This application has been tested to run on Safari and Firefox.

This project used git source control and information for this project can be viewed using the git command `git log`. See <https://git-scm.com> for git installation if needed.

### Installation

1. This application requires MeteorJS to be installed on the system. Please go to <https://www.meteor.com/install> to install Meteor.
2. Navigate to the location of this folder in the system's command prompt/line and run `meteor`. This will download any needed packages and get everything setup.
3. The application should now be running at <http://localhost:3000>. Point your browser to this address and start generating PDFs!

### Configuration

There are several options available when using webshot and these can be further
configured in the file `server/main.js` under the object "webshotOptions".

Be sure to remove/add the corresponding '/*' and '*/' around each option you would like to include/ignore.

### *Note*
As long as meteor is running, any saved changes to any file will force meteor to restart to capture this change. This is very helpful for quickly changing a setting, but if a PDF generation is currently running and meteor restarts, the PDF generation will be cancelled.
