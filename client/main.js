import { Template } from 'meteor/templating';

import './main.html';

Template.body.events({
  'click button#pdf' : function() {
    //Check for disabled button
    if ($("button#pdf").hasClass("disabled")) {
      return;
    }

    var url = $("input#url").val();

    //Check for url before sending to webshot
    if (url === "") {
      $("p#message").text("URL is empty");
      $("div#error").toggleClass("hidden");
      return;
    }

    //Disable button
    $("button").toggleClass("disabled");
    //Show running message
    $("div#runningMessage").toggleClass("hidden");
    //Hide error message
    if (!$("div#error").hasClass("hidden")) {
      $("div#error").addClass("hidden");
    }

    //Tell server to start the webshot
    Meteor.call("webshot", url, "pdf", function(error, result) {
      //Re-enable button
      $("button").toggleClass("disabled");
      //Hide runningMessage
      $("div#runningMessage").toggleClass("hidden");

      //Check for error
      if (error) {
        //Log error
	      console.error(error);
        //Show error message
        $("p#message").text(error);
        if ($("div#error").hasClass("hidden")) {
          $("div#error").removeClass("hidden");
        }
      } else if (result) { //otherwise display PDF in new tab
        //console.log("result = ", result);
	      window.open("data:application/pdf;base64, " + result, "result.pdf");
      }
    });
  }
});
